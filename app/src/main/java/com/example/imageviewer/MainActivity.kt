package com.example.imageviewer

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.helper.ItemTouchHelper
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager
import com.example.imageviewer.helpers.SimpleItemTouchHelperCallback
import com.example.imageviewer.helpers.SpaceItemDecorator
import com.example.imageviewer.ui.adapters.ImageAdapter
import com.example.imageviewer.viewmodels.ItemsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val itemsViewModel = ViewModelProviders.of(this)
                .get(ItemsViewModel::class.java)

        val spannedLayoutManager = SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 3)
        spannedLayoutManager.itemOrderIsStable = false
        recyclerview.layoutManager = spannedLayoutManager

        recyclerview.addItemDecoration(SpaceItemDecorator(1, 1, 1, 1))

        val adapter = ImageAdapter(this, itemsViewModel)
        recyclerview.adapter = adapter

        itemsViewModel.load()

        itemsViewModel.imageItemsData.observe(this, Observer {
            if (it?.isSuccess == true) {
                val res = it.result!!
                val data = res.items?.toCollection(ArrayList())
                if (data != null) {
                    (recyclerview.adapter as ImageAdapter).updateData(data)
                }
            }
        })
        itemsViewModel.addedItemData.observe(this, Observer {
            if (it?.isSuccess == true) {
                it.result?.let {
                    (recyclerview.adapter as ImageAdapter).addItem(it)
                }
            }
        })
        itemsViewModel.deletedItemData.observe(this, Observer {
            if (it?.isSuccess == true) {
                it.result?.let {
                    (recyclerview.adapter as ImageAdapter).deleteItem(it)
                }
            }
        })


//TODO properly implement Drag and drop support
        val callback = SimpleItemTouchHelperCallback(adapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(recyclerview)
    }
}
