package com.example.imageviewer.service;

import com.example.imageviewer.api.ApiResult
import com.example.imageviewer.model.ImageItems

interface ItemsService {
    fun addItem(uuid: String, url: String, callback: (ApiResult<ImageItems.Image>) -> Unit)
    fun loadJson(callback: (ApiResult<ImageItems>) -> Unit)
    fun deleteItem(uuid: String, position: Int, function: (ApiResult<Pair<String, Int>>) -> Unit)
}
