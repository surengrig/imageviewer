package com.example.imageviewer.service;

import android.content.Context
import com.example.imageviewer.api.ApiResult
import com.example.imageviewer.helpers.GsonUtil
import com.example.imageviewer.model.ImageItems
import com.google.gson.GsonBuilder
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets

class FileItemsService(private val context: Context) : ItemsService {

    private val gson = GsonBuilder().create()
    private val jsonFileName = "images.json"
    private var imageItems: ImageItems? = null

    override fun deleteItem(uuid: String, position: Int, function: (ApiResult<Pair<String, Int>>) -> Unit) {
        imageItems?.let {
            it.items?.removeAt(position)
            saveJsonAsync(it) {
                val result = if (it) ApiResult(Pair(uuid, position)) else ApiResult()
                function(result)
                return@saveJsonAsync
            }
        }
        function(ApiResult())
    }

    override fun addItem(uuid: String, url: String, callback: (ApiResult<ImageItems.Image>) -> Unit) {
        imageItems?.let {
            it.items?.add(ImageItems.Image(uuid, url))
            saveJsonAsync(it) {
                val result = if (it) ApiResult(ImageItems.Image(uuid, url)) else ApiResult()
                callback(result)
                return@saveJsonAsync
            }
        }
        callback(ApiResult())
    }

    override fun loadJson(callback: (ApiResult<ImageItems>) -> Unit) {
        readJsonAsync {
            if (it != null) {
                imageItems = it
                callback(ApiResult(it))
                return@readJsonAsync
            } else {
                imageItems = GsonUtil.itemsFromJson()
                imageItems?.let {
                    if (saveJson(it)) {
                        callback(ApiResult(it))
                        return@readJsonAsync
                    }
                }

            }
        }
        return
    }

    private fun readJson(): ImageItems? {
        try {
            context.openFileInput(jsonFileName).use { fileIn ->
                BufferedInputStream(fileIn, 65536)
                        .use { bufferedIn ->
                            InputStreamReader(bufferedIn, StandardCharsets.UTF_8).use { reader ->
                                return gson.fromJson(reader, ImageItems::class.java)
                            }
                        }
            }
        } catch (ex: Exception) {
        }

        return null
    }

    private fun saveJson(imageItems: ImageItems): Boolean {
        context.openFileOutput(jsonFileName, Context.MODE_PRIVATE).use { fileOut ->
            BufferedOutputStream(fileOut, 65536)
                    .use { bufferedOut ->
                        OutputStreamWriter(bufferedOut)
                                .use { writer ->
                                    gson.toJson(imageItems, writer)
                                    return true
                                }
                    }
        }
        return false
    }

    private fun saveJsonAsync(imageItems: ImageItems, callback: (Boolean) -> Unit) {
        doAsync {
            val result = saveJson(imageItems)
            uiThread {
                callback(result)
            }
        }
    }

    private fun readJsonAsync(callback: (ImageItems?) -> Unit) {
        doAsync {
            val result = readJson()
            uiThread {
                callback(result)
            }
        }
    }
}
