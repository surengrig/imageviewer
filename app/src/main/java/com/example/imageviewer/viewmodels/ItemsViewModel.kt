package com.example.imageviewer.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.example.imageviewer.api.ApiResult
import com.example.imageviewer.model.ImageItems
import com.example.imageviewer.service.FileItemsService
import com.example.imageviewer.service.ItemsService
import java.util.*


class ItemsRepository(context: Context) {
    private val itemsService: ItemsService

    init {
        itemsService = FileItemsService(context)
    }

    fun addItem(task: MutableLiveData<ApiResult<ImageItems.Image>>, url: String) {
        val uuid = UUID.randomUUID().toString()
        itemsService.addItem(uuid, url) {
            task.value = it
        }
    }

    fun deleteItem(data: MutableLiveData<ApiResult<Pair<String, Int>>>, uuid: String, adapterPosition: Int) {
        itemsService.deleteItem(uuid, adapterPosition) {
            data.value = it
        }
    }

    fun loadItems(task: MutableLiveData<ApiResult<ImageItems>>) {
        itemsService.loadJson {
            task.value = it
        }
    }
}


class ItemsViewModel(application: Application) : AndroidViewModel(application) {

    val deletedItemData: MutableLiveData<ApiResult<Pair<String, Int>>> = MutableLiveData()
    val imageItemsData: MutableLiveData<ApiResult<ImageItems>> = MutableLiveData()
    val addedItemData: MutableLiveData<ApiResult<ImageItems.Image>> = MutableLiveData()

    private val itemsRepository: ItemsRepository = ItemsRepository(application)

    fun deleteItem(uuid: String, adapterPosition: Int) {
        itemsRepository.deleteItem(deletedItemData, uuid, adapterPosition)
    }

    fun addItem(url: String) {
        itemsRepository.addItem(addedItemData, url)
    }

    fun load() {
        itemsRepository.loadItems(imageItemsData)
    }
}