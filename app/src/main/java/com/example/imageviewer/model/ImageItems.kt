package com.example.imageviewer.model

import com.google.gson.annotations.SerializedName

class ImageItems(@SerializedName("items") var items: MutableList<Image>?) {

    class Image {
        @SerializedName("imageUrlString")
        var url: String? = null
        @SerializedName("uuid")
        var uuid: String = ""

        constructor(uuid: String, url: String) {
            this.url = url
            this.uuid = uuid
        }
    }
}