package com.example.imageviewer.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.afollestad.materialdialogs.MaterialDialog
import com.arasthel.spannedgridlayoutmanager.SpanLayoutParams
import com.arasthel.spannedgridlayoutmanager.SpanSize
import com.bumptech.glide.Glide
import com.example.imageviewer.R
import com.example.imageviewer.helpers.ItemTouchHelperAdapter
import com.example.imageviewer.model.ImageItems
import com.example.imageviewer.viewmodels.ItemsViewModel
import java.util.*


class ImageAdapter(private val context: Context, private val viewModel: ItemsViewModel) :
        RecyclerView.Adapter<ViewHolder>(),
        ItemTouchHelperAdapter {

    private var imageList: MutableList<ImageItems.Image> = ArrayList()

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(imageList, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ViewType.ADD_BUTTON.ordinal -> {
                AddButtonViewHolder(layoutInflater.inflate(R.layout.list_item_add, parent, false))
            }
            else
            -> {
                ImageViewHolder(layoutInflater.inflate(R.layout.list_item_image, parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val width = if (position == 0) 2 else 1
        val height = if (position == 0) 2 else 1
        val spanSize = SpanSize(width, height)
        holder.itemView.layoutParams = SpanLayoutParams(spanSize)

        when (holder) {
            is ImageViewHolder -> {
                imageList[position].url?.let {
                    holder.loadImage(it)
                }

                holder.removeButton.setOnClickListener {
                    val adapterPosition = holder.adapterPosition
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        showRemoveDialog(adapterPosition)
                    }
                }

                holder.itemView.setOnClickListener {
                    val adapterPosition = holder.adapterPosition
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        showInfoDialog(adapterPosition)
                    }
                }
            }
            is AddButtonViewHolder -> {
                holder.itemView.setOnClickListener { showAddDialog() }
            }
        }
    }

    override fun getItemCount(): Int =
            imageList.size

    override fun getItemViewType(position: Int): Int =
            if (position == itemCount - 1) {
                ViewType.ADD_BUTTON.ordinal
            } else {
                ViewType.IMAGE.ordinal
            }

    fun updateData(data: List<ImageItems.Image>) {
        imageList = data as MutableList<ImageItems.Image>
        imageList.add(ImageItems.Image("", ""))
        notifyDataSetChanged()
    }


    fun addItem(image: ImageItems.Image) {
        imageList.add(imageList.size - 1, image)
        notifyItemInserted(imageList.size - 1)
    }

    fun deleteItem(item: Pair<String, Int>) {
        val (uuid, pos) = item
        val posToRemove = if (imageList[pos].uuid == uuid) {
            pos
        } else {
            imageList.indices.firstOrNull { imageList[it].uuid == uuid }
                    ?: -1
        }

        if (posToRemove > -1) {
            if (posToRemove == 0) {
                imageList[posToRemove] = imageList[posToRemove + 1]
                imageList.removeAt(posToRemove + 1)
                notifyItemChanged(posToRemove)
                notifyItemRemoved(posToRemove + 1)
            } else {
                imageList.removeAt(posToRemove)
                notifyItemRemoved(posToRemove)
            }
        }
    }


    private fun showInfoDialog(position: Int) = MaterialDialog.Builder(context)
            .content(context.getString(R.string.info_dialog_title_fmt, imageList[position].uuid))
            .positiveText(R.string.ok)
            .show()


    private fun showAddDialog() = MaterialDialog.Builder(context)
            .title(R.string.add_new_image)
            .negativeText(R.string.cancel)
            .positiveText(R.string.ok)
            .input(context.getString(R.string.add_image_hint), null, false) { dialog, input ->
                viewModel.addItem(input.toString())
            }
            .show()

    private fun showRemoveDialog(position: Int) = MaterialDialog.Builder(context)
            .content(context.getString(R.string.remove_dialog_title_fmt, imageList[position].uuid))
            .negativeText(R.string.cancel)
            .positiveText(R.string.ok)
            .onPositive { _, _ ->
                viewModel.deleteItem(imageList[position].uuid, position)

            }
            .show()


    inner class ImageViewHolder(itemView: View) : ViewHolder(itemView) {
        private val imageView: ImageView = itemView.findViewById(R.id.photo_imageview)
        val removeButton: ImageView = itemView.findViewById(R.id.remove_button)

        private var url = ""
        fun loadImage(url: String) {
            Glide.with(context)
                    .load(url)
                    .into(imageView)
            this.url = url
        }
    }

    inner class AddButtonViewHolder(itemView: View) : ViewHolder(itemView)

    enum class ViewType {
        ADD_BUTTON,
        IMAGE,
    }
}